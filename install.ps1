# This script installs "arflib-powershell" scripts into a standard
# location on Windows computers. The location is:
#       ${HOME}\psrcd
# Which is intentionally similar to Unix-like systems that use "rcd"
# directories for additional configuration and runtime files, along
# with the sister project "arflib-bash" (which installs to ${HOME}/bashrcd).
#
# Author: Alex Richard Ford (arf4188@gmail.com)

# Copy scripts into the "install" directory
$INSTALL_DIR = "${HOME}\psrcd"
Write-Output "Installing to: ${INSTALL_DIR}"
# TODO

# Insert loader code into $PROFILE.CurrentUserAllHosts
# Which should be something like: ${HOME}\Documents\WindowsPowerShell\profile.ps1
Write-Output "Inserting arflib-powershell loader code into $(${PROFILE}.CurrentUserAllHosts)"
# TODO

Write-Output "Finished!"