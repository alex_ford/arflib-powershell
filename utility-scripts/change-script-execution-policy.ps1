# By default, Windows systems restrict script execution for safety
# and security reasons. But in order to run any of my custom developed
# scripts, we must relax these restrictions. The following command
# will do that, just for the CurrentUser.

Set-ExecutionPolicy -Force -Scope CurrentUser -ExecutionPolicy Unrestricted
