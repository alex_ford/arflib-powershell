
# This function handles a very common sequence of operations that must happen
# prior to being able to use Docker on a Windows host when backed by Docker Machine.

function arf-docker-machine-env-init() {
    docker-machine start
    & docker-machine env | Invoke-Expression
}