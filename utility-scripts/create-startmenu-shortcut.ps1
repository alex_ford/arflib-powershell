<#
.SYNOPSIS
  Name: Create-StartMenuShortcut
  Wraps up the commands needed to create a new Start Menu shortcut on Windows.
  
.DESCRIPTION
  N/A
  
.PARAMETER ShortcutName
    The desired name to use for the shortcut.

.PARAMETER ExePath
    The actual path to the executable file.

.NOTES
    Updated: 2019-02-25        Initial creation.
    Release Date: N/A
   
    Author: Alex Richard Ford (arf4188@gmail.com)

.EXAMPLE
    Run the Create-StartMenuShortcut script to create a shortcut in the user's
    StartMenu:
    Create-StartMenuShortcut -ShortcutName Cmder -ExePath C:\tools\Cmder\Cmder.exe

#requires -version 2
#>

[CmdletBinding()]

PARAM ( 
    [string]$ShortcutName = $(throw "-ShortcutName is required."),
    [string]$ExePath = $(throw "-ExePath is required.")
)
#----------------[ Declarations ]------------------------------------------------------

# Set Error Action
# $ErrorActionPreference = "Continue"

# Dot Source any required Function Libraries
# . "C:\Scripts\Functions.ps1"

# Set any initial values
# $Examplefile = "C:\scripts\example.txt"
$START_MENU_DIR = "${HOME}\AppData\Roaming\Microsoft\Windows\Start Menu\"

#----------------[ Functions ]---------------------------------------------------------
Function Create-StartMenuShortcut {
    Param()
  
    Begin {
        If ( Test-Path -Path $ExePath -PathType Container ) {
            Write-Error "-ExePath must be an executable file, not a directory!"
            Exit 1
        } Else {
            # We are a file, let's proceed!
            Write-Output "ExePath: ${ExePath}"
        }
    }
  
    Process {
        Try{
            $WshShell = New-Object -comObject WScript.Shell
            $Shortcut = $WshShell.CreateShortcut("$START_MENU_DIR\$ShortcutName.lnk")
            $Shortcut.TargetPath = "$ExePath"
            $Shortcut.Save()
            Write-Output "StartMenu shortcut '${ShortcutName}' has been created at: ${START_MENU_DIR}\$ShortcutName.lnk"
        }
    
        Catch{
            Write-Error "Something went wrong."
            Break
        }

    }
  
    End {
        # only execute if the function was successful.
        If ( $? ) {
            Write-Host "$0 has finished!"
        }
    }
}

#----------------[ Main Execution ]----------------------------------------------------

# Script Execution goes here
Create-StartMenuShortcut