<#
.SYNOPSIS
    Convenience script that wraps a command line statement in calls to
    Push-Location and Pop-Location so that the command is executed within the
    directory specified.
.DESCRIPTION
    Long description
.EXAMPLE
    PS C:\> .\push-pop.ps1 . git status
    Explanation of what the example does
.INPUTS
    Inputs (if any)
.OUTPUTS
    Output (if any)
.NOTES
    General notes
#>
. .\shift-array.ps1
Push-Location $args[0]
$args = ShiftArray $args
$fp = $args[0]
$args = ShiftArray $args
# $OFS = " "
# $cmd = [string]$args
# Write-Host $cmd
Start-Process -NoNewWindow -Wait -FilePath $fp -ArgumentList $args
Pop-Location