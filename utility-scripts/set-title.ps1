# Makes it a tad bit easier to set the title of the Powershell terminal. Source
# this script into your current Powershell context:
#       . set-title.ps1
# Then use the function directly, like so:
#       Set-Title foobar
#
# Author: Alex Ford (arf4188@gmail.com)

function Set-Title {
    param (
        $TitleStr
    )
    $Host.UI.RawUI.WindowTitle = $TitleStr
}