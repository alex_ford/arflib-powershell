# Attempts to provide an emulated "ln" command using pure Windows capabilities.
# Requires administrator privs to run.
function ln([String] $Path1, [String] $Path2) {
    # Specifies a path to one or more locations.
    # Param(
    #     [Parameter(Mandatory=$true,
    #             HelpMessage="Path to one or more locations.")]
    #     [ValidateNotNullOrEmpty()]
    #     [String]
    #     $Path1,

    #     # LEFT-OFF-HERE: something isn't quite right about my syntax here.
    #     [Parameter(Mandatory=$true,
    #             HelpMessage="Path to one or more locations.")]
    #     [ValidateNotNullOrEmpty()]
    #     [String]
    #     $Path2
    # )
    Write-Output "Emulated 'ln' command for Native Windows."
    New-Item -ItemType SymbolicLink -Name ${Path1} -Target ${Path2}
}
