
function arf-get-system-uptime() {
    $UpTime = [Management.ManagementDateTimeConverter]::ToDateTime((Get-WmiObject Win32_OperatingSystem).LastBootUpTime)
    $TimeSpan = (Get-Date) - $UpTime
    return "{0:00} d {1:00} h {2:00} m {3:00} s" -f $TimeSpan.Days,$TimeSpan.Hours,$TimeSpan.Minutes,$TimeSpan.Seconds
}