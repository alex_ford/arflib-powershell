# This emulates the 'tail' command typically available on Unix-like systems.
#
# Simply put, this exists because PowerShell on Windows doesn't supply this in
# this familiar way/form.
#
# Author: Alex Richard Ford

# TODO: turn this into a function that can be sourced

Get-Content -Encoding UTF8 -Tail 25 $FILE
