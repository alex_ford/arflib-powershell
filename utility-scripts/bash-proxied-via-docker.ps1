# Usage:
#    Source this script, like this: . .\bash-proxied-via-docker.ps1

function bash() {
    docker-machine start
    Sleep 1
    & docker-machine env | Invoke-Expression
    Sleep 1
    docker run --rm -it `
        -v /c/Users:/c/Users `
        -w /c/Users/arf41 `
        bitnami/minideb `
        bash -c "cat /etc/os-release && bash"
}