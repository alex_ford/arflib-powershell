# Similar to the `add-to-path-temp.ps1` script, but this script adds the given
# argument to the system path **permanently**.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see LICENSE.md)

# function AddToPathPerm {
#     [CmdletBinding()]
#     param (

#     )

#     begin {
#         Write-Output "Hello, World!"
#     }

#     process {
#         Write-Output "Hello, World!"
#     }

#     end {
#     }
# }

Write-Output "WARNING this script is not ready for direct execution. Use as a reference-only for now!"
Exit 1

[Environment]::SetEnvironmentVariable("Path", $env:Path + ";c:\users\arf41\appdata\local\programs\python\python36\lib\site-packages", [EnvironmentVariableTarget]::Machine)

