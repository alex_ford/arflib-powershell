# Takes in a Windows style path (e.g. C:\Users\arf4188\) and converts it into
# a path suitable for passing into Docker, Docker-Compose, and Docker Machine.
# This primarily supports the use case of mounting the C:\ directory to the
# /c/ mount using VirtualBox on Windows.
#
# Usage: copy and paste as needed, or do an include by "dot sourcing"
#   . convert-path-to-docker.ps1
#
# One typical usage from a Powershell script is like this:
# ```
# ConvertPathToDocker($(Get-Location).Path)
# ```

# @author Alex Richard Ford (arf4188@gmail.com)

# TODO: Provide the mount point as defined in VirtualBox or Docker4Windows
#WIN_DOCKER_MOUNT_POINT

function ConvertPathToDocker($dirPath) {
    $dirPath = $dirPath.Replace(":", "")
    $dirPath = $dirPath.Replace("\", "/")
    $dirPath = $dirPath.Substring(0,1).ToLower() + $dirPath.Substring(1)
    $dirPath = $dirPath.Insert(0, "/")
    return $dirPath
}