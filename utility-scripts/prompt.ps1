# TODO: so this works, but it's pretty slow. The pipenv call is the culprit!
function prompt() {
    $prompt = ""
    $PIPENV_VERBOSITY = 1
    $PyVenv = Split-Path $(pipenv --venv) -Leaf
    if ($PyVenv) {
        $prompt = $prompt + "(" + $PyVenv + ")"
    }
    $prompt = $prompt + $(Get-Location) + "> "
    return $prompt
}