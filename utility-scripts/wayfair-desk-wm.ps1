<#
    For the following physical setup:
                      ___________  ___________
                     |           ||           |
     _______________ | Monitor 1 || Monitor 2 |
    |               ||___________||___________|
    | Laptop Screen |
    |_______________|
#>

# Moves Outlook 2016 to my laptop screen
Get-Process -Name outlook | Set-Window -X -1920 -Y 626 -Width 1536 -Height 824

Get-Process -Name slack | Set-Window -X -1920 -Y 626 -Width 1536 -Height 824
