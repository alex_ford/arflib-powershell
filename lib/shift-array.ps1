# Implements "shift" functionality over arrays in Powershell.
#
# Usage in other PowerShell scripts:
#   # import the shift-array.ps1 sniplet
#   . .\shift-array.ps1
#   # shift the values of a list/array, and store the result back to the same variable
#   $args = ShiftArray $args
#
# Testing
#   1. Open PowerShell terminal and source/import this script: . shift-array.ps1
#   2. Execute the included test function by simply calling it: testShiftArray
#
# @author Alex Richard Ford (arf4188@gmail.com)

function ShiftArray([string[]] $array) {
    # One element "shift"
    $null, $retval = $array
    return $retval
}

function testShiftArray() {
    $arr = "one", "two", "three"
    $arr = ShiftArray($arr)
    Write-Output $arr
    Write-Output ""
    $arr = ShiftArray($arr)
    Write-Output $arr
    Write-Output ""
    $arr = ShiftArray($arr)
    Write-Output "Finally: $arr"
}