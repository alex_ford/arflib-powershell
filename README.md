ARFLIB PowerShell
=================

A collection of Powershell functions and other snippets that are generally useful outside of any single project.

<table>
    <tr>
        <td><b>Author:</b></td>
        <td>Alex Richard Ford (arf4188@gmail.com)</td>
    </tr>
    <tr>
        <td><b>Website:</b></td>
        <td><a href="http://www.alexrichardford.com">http://www.alexrichardford.com</a></td>
    </tr>
    <tr>
        <td><b>License:</b></td>
        <td>MIT License (see <a href=".\LICENSE">.\LICENSE</a>)</td>
    </tr>
</table>

# Directory and File Structure

```shell
.\
.\.vscode\                Hidden/special directory for Visual Studio Code IDE files.
.\lib\                    Contains PowerShell scripts that are useful as included
                          functions in other scripts.
.\utility-scripts\        These are "end-user" PowerShell scripts, meant to be useful
                          when invoked directly from the terminal by a user.
.\install.ps1             (NOT CURRENTLY USED)
.\LICENSE                 Full license text for this project.
.\README.md               Primary starting place for documentation for this project.
.\uninstall.ps1           (NOT CURRENTLY USED)
```

# Separation of Lib and Utility-Scripts

The key thing to keep in mind here is that PowerShell code that is intended to be sourced/imported into other PowerShell scripts should go into the `.\lib\` directory. On the other hand, if the PowerShell script is meant to be used by the user, e.g. on the command line, then it should go into `.\utility-scripts\`.