<#
    When passing arguments from Powershell to a Python script, make sure to use
    the following syntax to avoid globbing the arguments into a single string.
    When this happens, the python script recieves a *single* argument (which is
    a globbed string of args) instead of a proper list/array of args.
#>

pipenv run python .\script.py $($args)
